//
//  ViewController.swift
//  memory4
//
//  Created by glenn on 1/6/19.
//  Copyright © 2019 glenn. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    deinit {
            print("bye ViewController")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        /*
        let personA = Person(name: "AAA")
        let personB = Person(name: "BBB")
        let personC = Person(name: "CCC")
        
        personA.friend = personB
        personB.friend = personC
        personC.friend = personA
    */
        print("setting element in v.d.s.")
        let element = HTMLelement(name: "ul", text: "smome test")
        print("now calling the html func")
         print(element.asHTML())
        
    }
}

class Person {
    
    var name:String
    weak var friend:Person?
    init(name:String) {
        self.name = name
    }
    deinit {
        print(" \(self.name) being deinitialized ")
    }
    
}

class HTMLelement {
    
    let name:String
    let text:String?
    //this is a closure object
    lazy var asHTML: ()->String = {
        
        [unowned htmlElement=self ] in
        print("html function now exists" )
        if let text = htmlElement.text {
            return "this is some text \(htmlElement.name) "
        } else {
            return "some text \(htmlElement.name)"
        }
        
        
    }
    
    init(name:String, text:String? ) {
       self.name = name
        self.text = text
        
    }
    
    
}

